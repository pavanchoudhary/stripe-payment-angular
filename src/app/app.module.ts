import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';

import { NgxStripeModule } from 'ngx-stripe';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, FormsModule, ReactiveFormsModule,
    NgxStripeModule.forRoot('pk_test_Js4DpNnsRihytVxGZQebCsQN'),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
